import processing.core.PVector;

public class Blokkie {
    PVector position;
    float width;
    float height;
    float depth;
    int color;
    boolean direction;
    boolean falling;
    StackStack main;

    public Blokkie(float width, float depth, float height, StackStack main, float x, float y, float z, boolean falling) {
        this.main = main;
        this.position = new PVector(x, y, z + 50);
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.color = main.color((int) (Math.random() * 400), 255, 255);
        this.falling = falling;
    }

    public Blokkie(Blokkie currentBlokkie) {
        this.main = currentBlokkie.main;
        this.position = currentBlokkie.position.copy();
        this.position.z += 50;
        this.width = currentBlokkie.width;
        this.height = currentBlokkie.height;
        this.depth = currentBlokkie.depth;
        this.color = main.color((int) (Math.random() * 400), 255, 255);
    }

    public Blokkie(Blokkie currentBlokkie, int increaseBy) {
        this.main = currentBlokkie.main;
        this.position = currentBlokkie.position.copy();
        this.position.z += 50;
        this.width = currentBlokkie.width + increaseBy;
        this.height = currentBlokkie.height;
        this.depth = currentBlokkie.depth + increaseBy;
        this.color = main.color((int) (Math.random() * 256), 255, 255);
    }

    public void draw() {
        main.pushMatrix();
        main.translate(position.x, position.y, position.z);
        main.fill(color);
        main.box(width, depth, height);
        main.popMatrix();
        if (position.x < -750 || position.x > 750 || position.y < -750 || position.y > 750) {
            direction = !direction;
        }
        if (falling) {
            position.z -= 10;
        }
    }
}
