import processing.core.*;

import java.util.ArrayList;

public class StackStack extends PApplet {

    public int points;
    public final int blokkieheight = 50;
    public ArrayList<Blokkie> blokkies;
    public Blokkie currentBlokkie;
    boolean direction;
    int streak;

    public static void main(String[] args) {
        PApplet.main("StackStack");
    }

    public void settings() {
        fullScreen(P3D);
    }

    public void setup() {
        noCursor();
        colorMode(PConstants.HSB);
        blokkies = new ArrayList<>();
        blokkies.add(new Blokkie(500, 500, blokkieheight, this, 0, 0, 0, false));
        currentBlokkie = new Blokkie(500, 500, blokkieheight, this, blokkies.get(blokkies.size() - 1).position.x, blokkies.get(blokkies.size() - 1).position.y, blokkies.get(blokkies.size() - 1).position.z, false);
    }

    public void draw() {
        background(0);
        camera(width / 2f, width / 2f, height / 2f + 3000 + (points * blokkieheight), 0, 0, (points * blokkieheight), 1, 1, 1);
        fill(255);
        textSize(500);
        text(points, 1000, 1000, 1000 + (points * blokkieheight));
        showOldBlokkies();
        showCurrentBlokkie();
    }

    public void placeBlokkie() {
        Blokkie prevBlokkie = blokkies.get(blokkies.size() - 1);
        points++;
        float overhang;
        if (direction) {
            overhang = abs(prevBlokkie.position.x - currentBlokkie.position.x);
            if (currentBlokkie.width < overhang) {
                currentBlokkie.width = 0f;
                gameOver();
                return;
            } else {
                currentBlokkie.width -= overhang;
            }

            if (currentBlokkie.position.x > prevBlokkie.position.x) {
                blokkies.add(new Blokkie(overhang, currentBlokkie.depth, currentBlokkie.height, this, currentBlokkie.position.x + overhang / 2, currentBlokkie.position.y, currentBlokkie.position.z, true));
                currentBlokkie.position.x -= overhang / 2;
            } else if (currentBlokkie.position.x < prevBlokkie.position.x) {
                blokkies.add(new Blokkie(overhang, currentBlokkie.depth, currentBlokkie.height, this, currentBlokkie.position.x - overhang / 2, currentBlokkie.position.y, currentBlokkie.position.z, true));
                currentBlokkie.position.x += overhang / 2;
            }
        } else {
            overhang = abs(prevBlokkie.position.y - currentBlokkie.position.y);
            if (currentBlokkie.depth < overhang) {
                currentBlokkie.depth = 0f;
                gameOver();
                return;
            } else {
                currentBlokkie.depth -= overhang;
            }
            if (currentBlokkie.position.y > prevBlokkie.position.y) {
                blokkies.add(new Blokkie(currentBlokkie.width, overhang, currentBlokkie.height, this, currentBlokkie.position.x, currentBlokkie.position.y + overhang / 2, currentBlokkie.position.z, true));
                currentBlokkie.position.y -= overhang / 2;
            } else if (currentBlokkie.position.y < prevBlokkie.position.x) {
                blokkies.add(new Blokkie(currentBlokkie.width, overhang, currentBlokkie.height, this, currentBlokkie.position.x, currentBlokkie.position.y - overhang / 2, currentBlokkie.position.z, true));
                currentBlokkie.position.y += overhang / 2;
            }
        }
        if (overhang < 10) {
            streak++;
        } else {
            streak = 0;
        }
        if (streak > 3) {
            currentBlokkie = new Blokkie(currentBlokkie, 100);
        }
        blokkies.add(currentBlokkie);
        currentBlokkie = new Blokkie(currentBlokkie);
    }

    public void showOldBlokkies() {
        for (Blokkie b : blokkies) {
            b.draw();
        }
    }

    public void showCurrentBlokkie() {
        if (direction) {
            if (currentBlokkie.direction) {
                currentBlokkie.position.x += 10;
            } else {
                currentBlokkie.position.x -= 10;
            }
        } else {
            if (currentBlokkie.direction) {
                currentBlokkie.position.y += 10;
            } else {
                currentBlokkie.position.y -= 10;
            }
        }
        currentBlokkie.draw();
    }


    public void keyPressed() {
        if (key == ' ') {
            placeBlokkie();
            direction = !direction;
        }
    }

    public void gameOver() {
        points = 0;
        blokkies.clear();
        blokkies.add(new Blokkie(500, 500, blokkieheight, this, 0, 0, 0, false));
        currentBlokkie = new Blokkie(500, 500, blokkieheight, this, blokkies.get(blokkies.size() - 1).position.x, blokkies.get(blokkies.size() - 1).position.y, blokkies.get(blokkies.size() - 1).position.z, false);
    }
}
